import { createStackNavigator } from 'react-navigation';
import LoginScreen from '../Screens/LoginScreen';
import EmployeeScreen from '../Screens/EmployeeScreen';

export default createStackNavigator(
    {
        Login: LoginScreen,
        Employee: EmployeeScreen,
    },
    {
        initialRouteName: 'Login',
    }
);