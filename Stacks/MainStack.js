import React from 'react';
import { createBottomTabNavigator } from 'react-navigation';
import Ionicons from 'react-native-vector-icons/Ionicons';
import PhotoScreen from '../Screens/PhotoScreen';
import MedicalScreen from '../Screens/MedicalScreen';
import PassScreen from '../Screens/PassScreen';
import ContactScreen from "../Screens/ContactScreen";
import NotesScreen from "../Screens/NotesScreen";

export default createBottomTabNavigator(
    {
        Photo: PhotoScreen,
        Medical: MedicalScreen,
        Pass: PassScreen,
        Contact: ContactScreen,
        Notes: NotesScreen
    },
    {
        navigationOptions: ({ navigation }) => ({
            tabBarIcon: ({ focused, tintColor }) => {
                const { routeName } = navigation.state;
                let iconName;
                if (routeName === 'Photo') {
                    iconName = `ios-happy${focused ? '' : '-outline'}`;
                } else if (routeName === 'Medical') {
                    iconName = `ios-medkit${focused ? '' : '-outline'}`;
                } else if (routeName === 'Pass') {
                    iconName = `ios-play${focused ? '' : '-outline'}`;
                } else if (routeName === 'Contact') {
                    iconName = `ios-call${focused ? '' : '-outline'}`;
                } else if (routeName === 'Notes') {
                    iconName = `ios-document${focused ? '' : '-outline'}`;
                }

                return <Ionicons name={iconName} size={30} color={tintColor} />;
            },
        }),
        tabBarOptions: {
            activeTintColor: 'red',
            inactiveTintColor: 'gray',
        },
    }
)
