import { createStackNavigator } from 'react-navigation';
import LoginStack from './LoginStack';
import MainStack from './MainStack';

export default createStackNavigator(
    {
        Login: LoginStack,
        Main: MainStack,
    },
    {
        initialRouteName: 'Login',
        mode: 'modal',
        headerMode: 'none',
    }
);