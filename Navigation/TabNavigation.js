import React from 'react';
import { createBottomTabNavigator } from 'react-navigation';
import Ionicons from 'react-native-vector-icons/Ionicons';
import HomeScreen from '../Screens/HomeScreen';
import ListScreen from '../Screens/ListScreen';

export default createBottomTabNavigator(
  {
    Home: HomeScreen,
    List: ListScreen,
  },
  {
    navigationOptions: ({ navigation }) => ({
        tabBarIcon: ({ focused, tintColor }) => {
            const { routeName } = navigation.state;
            let iconName;
            if (routeName === 'Home') {
              iconName = `ios-home${focused ? '' : '-outline'}`;
            } else if (routeName === 'List') {
              iconName = `ios-list${focused ? '' : '-outline'}`;
            }
            return <Ionicons name={iconName} size={30} color={tintColor} />;
        },
    }),
    tabBarOptions: {
      activeTintColor: 'black',
      inactiveTintColor: 'gray',
    },
  }
)