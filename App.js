import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import LoginSwitch from './Screens/LoginSwitch';
import LoginStack from './Stacks/LoginStack';
import RootStack from './Stacks/RootStack';

Text.defaultProps.allowFontScaling = false;

export default class App extends React.Component {

    componentDidMount() {
        
    }

    render() {
      return (
        <RootStack />
      );
    }
}