import { Platform } from 'react-native';
import GuestList from '../DummyData/Guests';
import _ from 'lodash';

export const GuestServiceFactory = (() => {
    this._currentGuest = null;
    this._allGuests = GuestList.Guests;
    return {
        setCurrentGuest: (id) => {
            this._currentGuest = _.find(this._allGuests, (guest) => { return guest.id === id; });
            return this._currentGuest != null;
        },
        getCurrentGuest: () => {
            return _currentGuest;
        },
        getGuest: (id) => {
            return _.find(this._allGuests, function (guest) {
                return guest.id == id;
            })
        }
    }
});

export const GuestService = GuestServiceFactory();
