import { Platform } from 'react-native';
import _ from 'lodash';
import EmployeeList from '../DummyData/Employees';

export const LoginServiceFactory = (() => {
    let _currentUser = null;
    let _lastUsedId = "";
    let _allEmployees = EmployeeList.employees;

    return {
        login: (id) => {
            _lastUsedId = id;
            _currentUser = _.find(_allEmployees, (employee) => { return employee.id === id; });
            return _currentUser != null;
            return true;
        },
        logout: () => {
            _currentUser = null;
        },
        getCurrentUser: () => {
            return _currentUser;
        },
        getCurrentUserName: () => {
            return _currentUser.name;
        },
        getLastUsedId: () => {
            return _lastUsedId;
        }
    };
});

export const loginService = LoginServiceFactory();
