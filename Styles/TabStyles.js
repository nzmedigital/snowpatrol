import React from 'react';
import {StyleSheet} from 'react-native';

export default styles = StyleSheet.create({
    container: {
      flex: 1,               
    },
    photo: {
      width: 200,
      height: 200,
      borderRadius: 200,
    },
    infoText: { 
      fontSize: 20,
      fontWeight: 'bold',
      paddingBottom: 15,
    },
    star: {
      width: 60,
      height: 60
    },
    content: {
        marginTop:30,
        paddingTop: 45,
        paddingBottom: 45,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center', 
    },
    header: {
        marginHorizontal: 10,
        backgroundColor: 'gray',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 10,
        height: 50,
        color: 'white',
        padding: 10,
        margin: 10
    },
});