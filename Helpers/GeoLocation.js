import { Platform } from 'react-native';

module.exports = {
  getUserLocation(successCallback, errorCallback) {
    const geoOptions =
      Platform.OS === 'ios'
        ? { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
        : {};
    navigator.geolocation.getCurrentPosition(
      (position) => {
        try {
          const pos = JSON.stringify(position);
          if (successCallback) {
            successCallback(pos);
          }
        } catch (error) {
          errorCallback(error);
        }
      },
      (error) => {
        if (errorCallback) {
          errorCallback(error);
        }
      },
      geoOptions,
    );
  },

  distanceBetweenTwoLocations(lon1, lat1, lon2, lat2) {
    const R = 6371; // Radius of the earth in km
    const dLat = this.toRad(lat2 - lat1);
    const dLon = this.toRad(lon2 - lon1);
    const a =
      Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.cos(this.toRad(lat1)) *
        Math.cos(this.toRad(lat2)) *
        Math.sin(dLon / 2) *
        Math.sin(dLon / 2);
    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    const d = R * c; // Distance in km

    return d;
  },

  getBound(lat, long, radiusInKM = 1, aspectRatio = 1) {
    const delta = this.getLatLongDelta(lat, long, radiusInKM, aspectRatio);
    const topRightLat = lat + delta.lat / 2.0;
    const topRightLong = long + delta.long / 2.0;
    const bottomLeftLat = lat - delta.lat / 2.0;
    const bottomLeftLong = long - delta.long / 2.0;
    const topLeftLat = lat + delta.lat / 2.0;
    const topLeftLong = long - delta.long / 2.0;
    const bottomRightLat = lat - delta.lat / 2.0;
    const bottomRightLong = long + delta.long / 2.0;

    return {
      topLeft: { lat: topLeftLat, long: topLeftLong },
      bottomRight: { lat: bottomRightLat, long: bottomRightLong },
      topRight: { lat: topRightLat, long: topRightLong },
      bottomLeft: { lat: bottomLeftLat, long: bottomLeftLong },
    };
  },

  getLatLongDelta(lat, long, radiusInKM, aspectRatio) {
    const earthRadiusInKM = 6371;
    const radiusInRad = radiusInKM / earthRadiusInKM;
    const longitudeDelta = this.toDeg(radiusInRad / Math.cos(this.toRad(lat)));
    const latitudeDelta = aspectRatio * this.toDeg(radiusInRad);

    return {
      lat: latitudeDelta,
      long: longitudeDelta,
    };
  },

  toRad(v) {
    return v * Math.PI / 180;
  },

  toDeg(v) {
    return v / Math.PI * 180;
  },

  getDealDistanceUnit(distance) {
    return distance < 1
      ? `${(distance * 1000).toFixed(1 / 10)}m`
      : `${distance.toFixed(1 / 10)}km`;
  },
};
