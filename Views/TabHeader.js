import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { ListItem, Button, Header } from 'react-native-elements';
import { GuestService } from '../Services/GuestService';

export default class TabHeader extends React.Component {

    onPressCloseButton = () => {
        if (this.props.closeAction) {
            this.props.closeAction();
        }
    }

    render() {
        const currentGuest = GuestService.getCurrentGuest();
        return (
            <Header
            leftComponent={
                <Button
                    title="Close"
                    buttonStyle={styles.buttonstyle}
                    clear
                    onPress={this.onPressCloseButton}
                />
            }
            centerComponent={{ text: currentGuest.name, style: styles.titleStyle }}
        />
        );
      }
}

const styles = StyleSheet.create({
    
    buttonstyle: {
        backgroundColor: 'transparent',
        padding: 0,
        left: -10,
        top: -20,
        position: 'absolute'
    },
    titleStyle: {
        color: '#fff',
        fontSize: 20,
        fontWeight: 'bold',
    },
});
