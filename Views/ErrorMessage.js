import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';

export default class ErrorMessage extends React.Component {

    static propTypes = {
        message: PropTypes.string
    }
    static defaultProps = {
        message: ""
    }

    render() {
        const { message } = this.props;
        return (
            <View>
                {!!message && message !== '' && 
                    <Text style={styles.inputError}>
                        {message}
                    </Text>                    
                }
            </View>
        );
    }
}

const styles = StyleSheet.create({
    inputError: {
        backgroundColor: '#f20',
        color: '#fff',
        marginBottom: 5,
    },
});
