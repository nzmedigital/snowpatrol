import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { loginService } from '../Services/LoginService';

export default class Banner extends React.Component {
    render() {
        const employeeName = loginService && loginService.getCurrentUserName();
        return (
            <View style={styles.container}>
                <Text style={styles.user}>
                    Employee: {employeeName}
                </Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        position: 'absolute',
        top: 69,
        left: 0,
        right: 0,
        backgroundColor: '#ddd',
        height: 30,
        zIndex: 2,
    },
    user: {
        marginTop: 5,
        alignSelf: 'flex-end',
        paddingRight: 10,
    }
});
