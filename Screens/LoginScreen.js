import React from 'react';
import { Image, View, Text, TextInput, StyleSheet } from 'react-native';
import { Button } from 'react-native-elements'
import { Constants, BarCodeScanner, Permissions } from 'expo';
import ErrorMessage from '../Views/ErrorMessage';
import { loginService } from '../Services/LoginService';

export default class LoginScreen extends React.Component {

    static _lastReadBarcode = "";

    static navigationOptions = {
        header: null,
    };

    constructor(props) {
        super(props);
        this.state = {
            employeeNo: loginService.getLastUsedId(),
            loginError: "",
            scanError: "",            
            scannerVisible: false,
            hasCameraPermission: null
        };
    }

    componentDidMount() {
        this._requestCameraPermission();
    }

    showError = (which, msg) => {
        this.setState({ [which]: msg });
    }

    onLoginPressed = () => {

        let employeeNo = this.state.employeeNo.trim();

        // Validation
        this.showError("loginError", "");
        if (employeeNo == "")
            this.showError("loginError", "Please enter an employee number.");

        // Login
        else if (loginService.login(employeeNo)) {
            this.setState({ scannerVisible: false });
            this.props.navigation.navigate('Employee');
        } else
            this.showError("loginError", "Could not find employee with given number.")
    }

    
    _handleBarCodeRead = data => {
        let barcode = data && data.data;
        if (barcode === "") return;

        if (this._lastReadBarcode === barcode) return;

        this._lastReadBarcode = barcode;
        if (!loginService.login(barcode))
            this.showError("scanError", "Could not recognise employee card");
        else {
            this.setState({ scannerVisible: false });
            this.showError("scanError", "")
            this.props.navigation.navigate('Employee');
        }            
    };



    _requestCameraPermission = async () => {
        const { status } = await Permissions.askAsync(Permissions.CAMERA);
        this.setState({
            hasCameraPermission: status === 'granted',
        });
    };


    render() {
        return (
            <View style={styles.container}>
                <Text></Text>   
                <Image
                    source={require("../Images/snowscan2.jpg")}
                    style={styles.loginScreenImage}
                    resizeMode="contain"
                />
                <Text style={styles.title}>Snow Scan</Text>

                <Text style={styles.instructions}>Please scan your Employee Pass to login.</Text>
                <View style={styles.formRow}>
                    <ErrorMessage message={this.state.loginError}></ErrorMessage>
                    <View style={styles.formElements}>
                        <TextInput
                            style={styles.input}
                            underlineColorAndroid={'transparent'}
                            maxLength={10}
                            keyboardType='numeric'
                            onChangeText={(employeeNo) => { this.showError(""); this.setState({ employeeNo }); }}
                            value={this.state.employeeNo}/>
                        <Button
                            title={'Login'}
                            onPress={this.onLoginPressed}/>
                    </View>
                </View>
                <Button onPress={() => { this.setState({ scannerVisible: !this.state.scannerVisible }) }} title={ this.state.scannerVisible ? 'Disable Barcode' : 'Enable Barcode' }/>
                <ErrorMessage message={this.state.scanError}></ErrorMessage>
                {this.state.hasCameraPermission === null ?
                    <Text>Requesting camera permission</Text> :
                    this.state.hasCameraPermission === false ?
                        <Text>Camera permission is not granted</Text> :
                        this.state.scannerVisible ?
                            <BarCodeScanner
                                onBarCodeRead={this._handleBarCodeRead}
                                style={{ height: 200, width: 200 }} 
                            />
                        : null
                }
            </View>
        );
    }
}

const styles = StyleSheet.create({
    loginScreenImage: {
        position: 'absolute',
        top:0
    },
    container: {
        flex: 1,
        //flexWrap: 'wrap',
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'space-around',
    },
    title: {
        textAlign: 'center',
        color: '#c0289f',
        fontWeight: 'bold',
        fontSize: 50,
    },
    welcome: {
        textAlign: 'center',
        color: 'blue',
        fontWeight: 'bold',
        fontSize: 25,
    },
    instructions: {
        textAlign: 'center',
        color: 'black',
        fontWeight: 'bold',
        fontSize: 15,
    },
    input: {
        width: 150,
        borderColor: 'gray',
        borderWidth: 1,
        height: 50,
        backgroundColor: 'white'
    },
    formRow: {
        flexDirection: 'column',
        justifyContent: 'center'
    },   
    formElements: {
        justifyContent: 'center',
        flexDirection: 'row',        
    }
});
