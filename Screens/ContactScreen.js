import React from 'react';
import { FlatList, ScrollView, View, Text, StyleSheet, Linking } from 'react-native';
import { ListItem, Button, Header } from 'react-native-elements';
import Banner from '../Views/Banner';
import TabHeader from '../Views/TabHeader';
import { GuestService } from '../Services/GuestService';
import { loginService } from '../Services/LoginService';

export default class ContactScreen extends React.Component {

    constructor() {
        super();
        this.state = {}
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return null;
    }

    componentDidUpdate(prevProps, prevState) {

    }

    onPressUser = (item) => {
        Linking.openURL(`tel:${item.phone}`);
    }

    renderEmContactItem = ({item}) => {
        return (
           <ListItem
               title={item.name}
               subtitle={item.phone + " " + item.email}
               onPress={() => this.onPressUser(item)}
               />
        )
    }

    renderAssociateItem = ({item}) => {
        var associate = GuestService.getGuest(item.id)
        return (
            <ListItem
                title={associate.name + " (" + item.relationship + ")"}
                subtitle={associate.phone}
                onPress={() => this.onPressUser(associate)}
            />
        )
    }

    render() {
        var currentGuest = GuestService.getCurrentGuest();
        return (
            <View style={styles.container}>
                <TabHeader closeAction={this.props.navigation.pop}/>
                <Banner />
                <View style={styles.content}>
                    <View>
                        <Text style={styles.header}>Emergency Contact Details</Text>
                        <FlatList
                            data={currentGuest.emergencycontact}
                            renderItem={this.renderEmContactItem}
                            keyExtractor={(item, index) => index.toString()}
                        />
                    </View>
                    <View>
                        <Text style={styles.header}>Associates</Text>
                        <FlatList
                            data={currentGuest.associates}
                            renderItem={this.renderAssociateItem}
                            keyExtractor={(item, index) => index.toString()}
                        />
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    emergencyContactContainerStyle: {
        backgroundColor: '#ccc',
    },
    header: {
        marginHorizontal: 10,
        backgroundColor: 'gray',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 10,
        height: 50,
        color: 'white',
        padding: 10,
        margin: 10
    },
    content: {
        marginTop: 30,
    },
});