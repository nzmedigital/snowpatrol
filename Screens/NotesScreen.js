import React from 'react';
import { FlatList, View, Text, StyleSheet, TextInput, RefreshControl } from 'react-native';
import { Button, ListItem, Header } from 'react-native-elements';
import Banner from '../Views/Banner';
import TabHeader from '../Views/TabHeader';
import { GuestService } from '../Services/GuestService';
import Modal from 'react-native-modal'; // 2.4.0


export default class NotesScreen extends React.Component {

    static navigationOptions = {
        title: 'Notes',
    };

    constructor() {
        super();
        const guestNotes = GuestService.getCurrentGuest() && GuestService.getCurrentGuest().notes;
        this.state = {
            modalVisible: false,
            isRefreshing: false,
            newNote: "",
            guestNotes: guestNotes
        }
    }

    componentDidMount() {

    }

    componentWillUnmount() {

    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return null;
    }

    componentDidUpdate(prevProps, prevState) {

    }

    listRefreshed = () => {
        this.setState({isRefreshing: false});
    }

    renderNoteItem = ({item}) => {
        return (
            <ListItem
                title={item.content}
            />
        )
    }

    addNotePressed = () => {
        this.setState({ modalVisible: true });
    }

    saveNotePressed = () => {
        const newNote = {"type":"1", "content":this.state.newNote};
        const curGuestNotes = this.state.guestNotes;
        curGuestNotes.push(newNote);
        this.setState({
            modalVisible: false,
            guestNotes: curGuestNotes,
        });
    }


    render() {
        return (
            <View style={styles.container}>

                <TabHeader closeAction={this.props.navigation.pop}/>
                <Banner />
                <View style={styles.content}>
                    <View>
                        <Text style={styles.header}>Notes</Text>
                        <FlatList
                            refreshControl={
                                <RefreshControl
                                    refreshing={this.state.isRefreshing}
                                    onRefresh={this.listRefreshed}
                                />
                            }
                            data={this.state.guestNotes}
                            renderItem={this.renderNoteItem}
                            keyExtractor={(item, index) => index.toString()}
                        />
                    </View>
                    <Button
                        style={styles.button}
                        title={'Add note'}
                        onPress={this.addNotePressed}
                        />
                </View>
                <Modal
                    style={styles.modalContent}
                    isVisible = {this.state.modalVisible}>
                    <View>
                        <TextInput
                            style={styles.input}
                            underlineColorAndroid={'transparent'}
                            multiline={true}
                            numberOfLines={10}
                            onChangeText={(newNote) => { this.setState({ newNote }); }}
                            value={this.state.newNote}/>
                        <Button
                            style={styles.button}
                            title={'Save note'}
                            onPress={this.saveNotePressed}
                        />
                    </View>
                </Modal>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    content: {
        marginTop: 30,
    },
    header: {
        marginHorizontal: 10,
        backgroundColor: 'gray',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 10,
        height: 50,
        color: 'white',
        padding: 10,
        margin: 10
    },
    button: {
        margin: 10
    },
    modalContent: {
        backgroundColor: 'white',
        padding: 22,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 4,
        borderColor: 'rgba(0, 0, 0, 0.1)',
    },
    input: {
        borderColor: 'black'
    }
});