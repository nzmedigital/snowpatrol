import React from 'react';
import { createSwitchNavigator } from 'react-navigation';
import MainStack from '../Stacks/MainStack';
import LoginScreen from '../Screens/LoginScreen';

export default createSwitchNavigator(
    {
    	Login: LoginScreen,
    	MainStack: MainStack,
    },
    {
    	initialRouteName: 'Login'
    }
);