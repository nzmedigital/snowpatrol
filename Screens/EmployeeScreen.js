import React from 'react';
import { ScrollView, View, Text, TextInput, StyleSheet, Image, KeyboardAvoidingView } from 'react-native';
import { Button, Avatar, Divider } from 'react-native-elements';
import { Constants, BarCodeScanner, Permissions } from 'expo';
import ErrorMessage from '../Views/ErrorMessage';
import { loginService } from '../Services/LoginService';
import { GuestService } from '../Services/GuestService';
import { Table, TableWrapper, Row, Rows, Col } from 'react-native-table-component';

export default class EmployeeScreen extends React.Component {

  constructor(props) {
      super(props);

      const 
        image1 = (<Image
          style={styles.star}
          source={{uri: 'http://releases.connectintouch.com/geekweek/badges/star_bronze.png'}}
        />);
        image2 = (<Image
          style={styles.star}
          source={{uri: 'http://releases.connectintouch.com/geekweek/badges/star_silver.png'}}
        />);
        image3 = (<Image
          style={styles.star}
          source={{uri: 'http://releases.connectintouch.com/geekweek/badges/star_gold.png'}}
        />);

      this.state = {
        tableHead: [' ', '  0-10', ' 11-20', ' 21-35', ' 36-50', ' 51-65', ' 66+'],
        tableTitle: [' M', ' F'],
        tableData: [[image1, '', image2, image3, '', image1],[image3, image1, '', '', '', image2]], 
        employeeNo: "",
        guestIDError: "",
        scanError: "",            
        scannerVisible: false,
        hasCameraPermission: null
      };
  }

  componentDidMount() {
    this._requestCameraPermission();
  }

  showError = (which, msg) => {
    this.setState({ [which]: msg });
  }

  onGuestLookup = () => {

    let guestID = this.state.guestID;

    // Validation / Login
    this.showError("guestIDError", "");
    if (guestID == "") 
      this.showError("guestIDError", "Please enter an guest card number.");
    else if (GuestService.setCurrentGuest(guestID))
      this.props.navigation.navigate('Main');
    else
      this.showError("guestIDError", "Could not find guest with given number.")
  }

  onLogout = () => {
    loginService.logout();
    this.props.navigation.navigate('Login');
  }

  _handleBarCodeRead = data => {
    let barcode = data && data.data;
    if (barcode === "") return;

    if (this._lastReadBarcode === barcode) return;

    this._lastReadBarcode = barcode;
    if (!GuestService.setCurrentGuest(barcode))
        this.showError("scanError", "Could not recognise guest card.");
    else {
        this.setState({ scannerVisible: false });
        this.showError("scanError", "")
        this.props.navigation.navigate('Main');
    }            
  };

  _requestCameraPermission = async () => {
    const { status } = await Permissions.askAsync(Permissions.CAMERA);
    this.setState({
        hasCameraPermission: status === 'granted',
    });
  };

  render() {
    return (
      <View style={styles.container}>

        <Image
            style={styles.avatar}
            source={{ uri: loginService.getCurrentUser().avatar }}
        />       

        {this.state.scannerVisible ?
          <BarCodeScanner
            onBarCodeRead={this._handleBarCodeRead}
            style={{ height: 200, width: 200 }} 
          />
        :<View> 
          <View style={{
            flexDirection: 'row',
            }}
          >
            <Image
              style={styles.badge}
              source ={loginService.getCurrentUser().badges.cancel === 'Yes'?
                {uri: 'http://releases.connectintouch.com/geekweek/badges/cancel.png'}:
                {uri: 'http://releases.connectintouch.com/geekweek/badges/cancel_g.png'}
              }
            />
            <Image
              style={styles.badge}
              source ={loginService.getCurrentUser().badges.commend === 'Yes'?
                {uri: 'http://releases.connectintouch.com/geekweek/badges/commend.png'}:
                {uri: 'http://releases.connectintouch.com/geekweek/badges/commend_g.png'}
                }
            />
            <Image
              style={styles.badge}
              source ={loginService.getCurrentUser().badges.dummy === 'Yes'?
                {uri: 'http://releases.connectintouch.com/geekweek/badges/dummy.png'}:
                {uri: 'http://releases.connectintouch.com/geekweek/badges/dummy_g.png'}
              }
            />
            <Image
              style={styles.badge}
              source ={loginService.getCurrentUser().badges.emergency === 'Yes'?
                {uri: 'http://releases.connectintouch.com/geekweek/badges/emergency.png'}:
                {uri: 'http://releases.connectintouch.com/geekweek/badges/emergency_g.png'} 
              }   
            />
            <Image
              style={styles.badge}
              source ={loginService.getCurrentUser().badges.note === 'Yes'?
                {uri: 'http://releases.connectintouch.com/geekweek/badges/note.png'}:
                {uri: 'http://releases.connectintouch.com/geekweek/badges/note_g.png'}
              }                
            />
            <Image
              style={styles.badge}
              source ={(loginService.getCurrentUser().badges.cancel === 'Yes')&&
                        (loginService.getCurrentUser().badges.commend === 'Yes')&&
                        (loginService.getCurrentUser().badges.dummy === 'Yes')&&
                        (loginService.getCurrentUser().badges.emergency === 'Yes')&&   
                        (loginService.getCurrentUser().badges.note === 'Yes')?
                {uri: 'http://releases.connectintouch.com/geekweek/badges/cookie.png'}:
                {uri: 'http://releases.connectintouch.com/geekweek/badges/cookie_g.png'}
              }
            />
          </View>
                  
          <View style={styles.tableContainer}>
            <Table>
              <Row data={this.state.tableHead} flexArr={[1, 1, 1, 1, 1, 1, 1]} 
                        style={styles.tableHead} textStyle={styles.tableText}/>
              <TableWrapper style={styles.tableWrapper}>
                <Col data={this.state.tableTitle} style={styles.title} heightArr={[40,40]} 
                        textStyle={styles.tableText}/>
                <Rows data={this.state.tableData} 
                        flexArr={[1, 1, 1, 1, 1, 1]} 
                        style={styles.tableRow} 
                        textStyle={styles.tableText}/>
              </TableWrapper>
            </Table>
          </View>
        </View>
        }

        <Text style={styles.instructions}>Scan or enter Guest Pass</Text>
        <ErrorMessage message={this.state.guestIDError}></ErrorMessage>
        <View style={styles.loginBar}>     
          <TextInput
            style={styles.input}
            underlineColorAndroid={'transparent'}
            maxLength={10}
            keyboardType='numeric'
            onChangeText={(guestID) => { this.setState({guestID}); }}
            value={this.state.guestID}
          />
          <Button
            title={'Scan'}
            onPress={this.onGuestLookup}/>
        </View>
        <Text></Text>

        <Button onPress={() => { this.setState({ scannerVisible: !this.state.scannerVisible }) }} title={ this.state.scannerVisible ? 'Disable Barcode' : 'Enable Barcode' }/>
          <ErrorMessage message={this.state.scanError}></ErrorMessage>
          {this.state.hasCameraPermission === null ?
            <Text>Requesting camera permission</Text> :
            this.state.hasCameraPermission === false ?
              <Text>Camera permission is not granted</Text> 
              :null
          }    

        <Button title="Log out" onPress={this.onLogout} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
      flex: 1,
      //flexWrap: 'wrap',
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'space-around',
    },
    avatar: { width: 120, height: 120, borderRadius: 120 },
    badge: { width: 56, height: 70 },
    star: { width: 30, height: 30 },
      welcome: {
      textAlign: 'center',
      color: 'blue',
      fontWeight: 'bold',
      fontSize: 25,
    },
    instructions: {
      textAlign: 'center',
      color: 'green',
      fontWeight: 'bold',
      fontSize: 15,
    },
    input: {
      width: 150,
      borderColor: 'gray',
      borderWidth: 1,
      height: 50
    },
    loginBar: { justifyContent: 'center', flexDirection: 'row' },
    tableContainer: { width: 346, padding: 16, paddingTop: 30, backgroundColor: '#fff' },
    tableHead: {  height: 28,  backgroundColor: '#f1f8ff'  },
    tableWrapper: { flexDirection: 'row' },
    tableTitle: { width: 346, backgroundColor: '#f1f8ff' },
    tableRow: {  height: 40 },
    tabeText: { textAlign: 'center', fontSize: 12 }
});
