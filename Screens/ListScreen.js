import React from 'react';
import { ScrollView, View, FlatList, Text, StyleSheet, Linking } from 'react-native';
import { List, ListItem, SearchBar } from 'react-native-elements';
import _ from 'lodash';
import UserList from '../DummyData/UserList';

export default class ListScreen extends React.Component {
    
    constructor() {
        super();
        this.allUsers = UserList.users;
        this.state = {
            displayUsers: this.allUsers,
        }
    }

    onChangeSearchBar = (value) => {
        const { displayUsers } = this.state;
        const filteredUsers = _.filter(this.allUsers, (user, index) => {
            const filter = value.toUpperCase();
            const name = user.name;
            return (name.toUpperCase().indexOf(filter) > -1);
        });

        this.setState({displayUsers: filteredUsers})
    }

    onClearSearchBar = () => {

    }

    onPressUser = (item) => {
        Linking.openURL(`tel:${item.phone}`);
    }

    renderItem = ({item}) => {
        return (
            <ListItem
                roundAvatar
                title={item.name}
                onPress={() => this.onPressUser(item)}
            />
        )
    }

    render(){
        return (
            <View style={styles.container}>
                <SearchBar
                    autoCorrect={false}
                    autoFocus={true}
                    lightTheme
                    onChangeText={this.onChangeSearchBar}
                    onClearText={this.onClearSearchBar}
                    placeholder="Search..."
                />
                <FlatList
                    renderItem={this.renderItem}
                    data={this.state.displayUsers}
                    keyExtractor={(item, index) => index.toString()}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
});