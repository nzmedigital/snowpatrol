import React from 'react';
import { View, ScrollView, Text, StyleSheet, SectionList, Alert } from 'react-native';
import { Button, Header } from 'react-native-elements';
import Banner from '../Views/Banner';
import TabHeader from '../Views/TabHeader';
import _ from 'lodash';
import { GuestService } from '../Services/GuestService';
import styles from '../Styles/TabStyles';


export default class PassScreen extends React.Component {

    static navigationOptions = {
        title: 'Pass',
    };

    constructor(props) {
        super(props);
    }

    render() {
        var currentGuest = GuestService.getCurrentGuest();
        return (
            <View style={styles.container}>
                <TabHeader closeAction={this.props.navigation.pop}/>
                <Banner />
                <View style={pageStyles.buttonHolder}>
                    <Button title="Cancel Pass" onPress={() => { Alert.alert('Cancel Pass', 'Are you sure to cancel this pass?', [{ text: "Yes, Cancel Pass" }, { text: "No, Keep Pass Valid" }]); }}/>
                </View>
                <ScrollView style={pageStyles.listContent} contentContainerStyle={pageStyles.listContainer}>
                    {
                        currentGuest.products &&
                        currentGuest.products.length > 0 ?
                        <SectionList
                            renderItem={({ item, index, section }) => {
                                let date = new Date(item["date-time"]);
                                let isOK = item["status"] === "OK";
                                return <View style={pageStyles.item}>
                                    <Text key={index} style={{ fontSize: 17 }}>{date.toLocaleString("en-US")}</Text>
                                    <Text style={{ fontSize: 17, color: isOK ? '#0A0' : '#A00' }}>{item["status"]}</Text>
                                </View>;
                            }}
                            renderSectionHeader={({ section: { sectionData } }) => {
                                return <Text style={styles.header}>{sectionData.description}</Text>
                            }}
                            sections={_.map(currentGuest.products, (product, key, products) => {
                                return {
                                    sectionData: product,
                                    data: product["last-scans"]
                                }
                            })}
                            keyExtractor={(item, index) => item + index}
                        /> :
                        <Text>No passes!</Text>
                    }
                </ScrollView>
            </View>
        );
    }
}

const pageStyles = StyleSheet.create({
    buttonHolder: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 100,
        padding: 10,
        flex: 1,
        flexDirection: 'column',
        alignItems: 'flex-end',
        justifyContent: 'space-between',
        height: 60
    },
    listContent: {
        marginTop: 90,
    },
    listContainer: {
        alignItems: 'stretch' 
    },
    section: {
        alignSelf: 'flex-start',
        fontWeight: 'bold',
        padding: 10
    },
    item: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 20,
        paddingRight: 20
    },
    header: {
        marginHorizontal: 10,
        backgroundColor: 'gray',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 10,
        height: 50,
        color: 'white',
        padding: 10,
        margin: 10
    },
});

