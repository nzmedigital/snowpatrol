import React from 'react';
import {View, Text, StyleSheet, Image} from 'react-native';
import {Card, Button, Header, Avatar, Divider} from 'react-native-elements';
import {loginService} from '../Services/LoginService';
import Banner from '../Views/Banner';
import TabHeader from '../Views/TabHeader';
import {NavigationActions } from 'react-navigation';
import {GuestService} from '../Services/GuestService';
import styles from '../Styles/TabStyles';

export default class PhotoScreen extends React.Component {

  constructor() {
    super();
    this.state = {
    }
  }

  componentDidMount() {

  }

  componentWillUnmount() {

  }

  static getDerivedStateFromProps(nextProps, prevState) {
    return null;
  }

  componentDidUpdate(prevProps, prevState) {
      
  }

  render() {
        var currentGuest = GuestService.getCurrentGuest();
        return (
            <View style={styles.container}>
              <TabHeader closeAction={this.props.navigation.pop}/>
              <Banner />
              <Card containerStyle={styles.content}>
                  <Avatar
                      rounded
                      xlarge
                      source={{ uri: currentGuest.avatar }}
                      style={styles.photo}
                  />                  
              </Card>
              <Card style={{ alignItems: 'center', justifyContent: 'center'}}>
                  <Text style={styles.infoText}>Name: {currentGuest.name}</Text>
                  <Text style={styles.infoText}>Age: {currentGuest.age}</Text>
                  <Text style={styles.infoText}>Gender: {currentGuest.gender}</Text>
              </Card>
            </View>
        );
  }
}