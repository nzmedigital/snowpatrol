import React from 'react';
import { createBottomTabNavigator } from 'react-navigation';
import Ionicons from 'react-native-vector-icons/Ionicons';
import PhotoScreen from '../Screens/PhotoScreen';
import DetailsScreen from '../Screens/DetailsScreen';
import PassScreen from '../Screens/PassScreen';
import ContactScreen from "./ContactScreen";
import NotesScreen from "./NotesScreen";


export default createBottomTabNavigator(
    {
        Photo: PhotoScreen,
        Details: DetailsScreen,
        Pass: PassScreen,
        Contact: ContactScreen,
        Notes: NotesScreen
    },
    {
        navigationOptions: ({ navigation }) => ({
            tabBarIcon: ({ focused, tintColor }) => {
                const { routeName } = navigation.state;
                let iconName;
                if (routeName === 'Photo') {
                    iconName = `ios-happy${focused ? '' : '-outline'}`;
                } else if (routeName === 'Details') {
                    iconName = `ios-list${focused ? '' : '-outline'}`;
                } else if (routeName === 'Pass') {
                    iconName = `ios-play${focused ? '' : '-outline'}`;
                }
                return <Ionicons name={iconName} size={30} color={tintColor} />;
            },
        }),
        tabBarOptions: {
            activeTintColor: 'red',
            inactiveTintColor: 'gray',
        },
    }
)