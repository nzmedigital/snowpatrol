import React from 'react';
import { View, Text, StyleSheet, FlatList } from 'react-native';
import { Button, Header, ListItem } from 'react-native-elements';
import Banner from '../Views/Banner';
import TabHeader from '../Views/TabHeader';
import { GuestService } from '../Services/GuestService';

export default class MedicalScreen extends React.Component {

    static navigationOptions = {
        title: 'Medical',
    };

    constructor() {
        super();
        this.state = {
        }
    }

    componentDidMount() {

    }

    componentWillUnmount() {

    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return null;
    }

    componentDidUpdate(prevProps, prevState) {

    }
    
    renderMedicalItem = ({item}) => {
        return (
            <ListItem
                title={item.type + ": " + item.content}
            />
        )
    }

    render() {
        var currentGuest = GuestService.getCurrentGuest();
        return (
            <View style={styles.container}>
                <TabHeader closeAction={this.props.navigation.pop}/>
                <Banner />
                <View style={styles.content}>
                    <Text style={styles.header}>Medical Details</Text>
                    <FlatList
                        data={currentGuest.medical}
                        renderItem={this.renderMedicalItem}
                        keyExtractor={(item, index) => index.toString()}
                    />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    content: {
        marginTop: 30,
    },
    header: {
        marginHorizontal: 10,
        backgroundColor: 'gray',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 10,
        height: 50,
        color: 'white',
        padding: 10,
        margin: 10
    },
});
